# OpenWeather Bot

This bot will collect data from [openweathermap](https://openweathermap.org/) web and create a simple api to work
with.

## Usage

1. Get your APIKey from [openweathermap](https://openweathermap.org/) place it 
    on -> app/config.ini
2. Add Your mongo to app/config.ini
3. Download [All City Data](http://bulk.openweathermap.org/sample/) place it on ->
    app/data
4. Build -> `docker build -t weather_bot .`
5. Run -> `docker run -d --name weather_bot -p 5000:5001 weather_bot`
6. Simple Usage -> http://localhost:5001/weather/tehran

## Maintainers

- Armin Sadreddin
- Alireza Sadraii