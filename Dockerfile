FROM python:3

ADD ./app /app

WORKDIR /app

RUN pip install --requirement ./requirements.txt

CMD [ "python", "./main.py" ]
