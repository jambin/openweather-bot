from datetime import datetime

from db import global_init
import configparser
import logging
import json
import pyowm
from flask_api import FlaskAPI, status
from services.CityService import CityService
from services.WeatherService import WeatherService

app = FlaskAPI(__name__)
app.config.update(
    DEBUG=False,
    TESTING=False)

# ------------------------------------------------------------------------------------------------------
# Configs
# Logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)
logger = logging.getLogger(__name__)

# Configuring bot
try:
    config = configparser.ConfigParser()
    config.read_file(open('config.ini'))
    owm = pyowm.OWM(config['DEFAULT']['api_key'])
except IOError:
    logger.error("Cant Open The Config File !")
    exit(1)


@app.route('/weather/<string:city_name>', methods=['GET'])
def check_weather(city_name):
    db_city = CityService.find_city(city_name)
    global find_weather
    if db_city is not None:
        find_weather = WeatherService.find_weather(db_city.id)
        if find_weather is None or (datetime.now() - find_weather.check_time).seconds / 3600 >= 1:
            try:
                observation = owm.weather_at_place(db_city.name)
            except Exception as e:
                logger.error("can't fetch from API.")
                return {'msg': "Weather API Failed"}, status.HTTP_500_INTERNAL_SERVER_ERROR
            fetch_weather = observation.get_weather()
            find_weather = WeatherService.add_weather(fetch_weather, db_city)
            logger.info(city_name + " new Weather added.")
        else:
            logger.info(city_name + "Weather loaded from DB")
    else:
        logger.error(f"city {city_name} not found.")
        return {'msg': 'City Not Found'}, status.HTTP_404_NOT_FOUND
    return WeatherService.out_weather(find_weather), status.HTTP_200_OK


def main():
    logger.info("Bot Starts !")
    try:
        global_init(config['DEFAULT']['db'], 'weather_api')
        logger.info("DB Inited !")
    except Exception as e:
        logger.error("DB Init FAIL !")
        exit(1)
    try:
        if config['DEFAULT']['add_city'] == 'True':
            logger.info('Adding Cities to DB')
            data = None
            with open(config['DEFAULT']['city_list']) as f:
                data = json.load(f)
            counter = 0
            total = len(data)
            if data is not None:
                if CityService.count_cities() != total:
                    logger.info('Cities is not same as DB')
                    for city in data:
                        logger.info(
                            'Adding ' + city['name'] + ' / ' + city['country'] + " (" + str(counter) + " / " + str(
                                total) + " )")
                        CityService.add_city(city)
                        counter += 1
                    config['DEFAULT']['add_city'] = 'False'
                    with open('config.ini', 'w') as configfile:  # save
                        config.write(configfile)
    except Exception as e:
        logger.error("Error in Adding Cities")

    app.run(debug=False)
    # Main Class Call


if __name__ == "__main__":
    main()
