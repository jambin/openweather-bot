from datetime import date, datetime

from models.City import City


class CityService:
    @classmethod
    def count_cities(cls):
        return City.objects().count()

    @classmethod
    def add_city(cls, city):
        c = City()
        c.city_id = city['id']
        c.name = city['name']
        c.country = city['country']
        c.pos = (float(city['coord']['lon']), float(city['coord']['lat']))
        c.save()
        return c

    @classmethod
    def find_city(cls, city_name: str):
        return City.objects(name__iexact=city_name).first()
