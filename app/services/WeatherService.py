from datetime import date, datetime

from models.City import City
from models.Weather import Weather


class WeatherService:
    @classmethod
    def find_weather(cls, city_id):
        return Weather.objects(city_id=city_id).first()

    @classmethod
    def add_weather(cls, input_weather, city: City):
        w = Weather()
        w.city_id = city.id
        w.humidity = input_weather.get_humidity()
        w.detailed_status = input_weather.get_detailed_status()
        w.temperature = input_weather.get_temperature(unit='celsius')['temp']
        w.max_temperature = input_weather.get_temperature(unit='celsius')['temp_max']
        w.min_temperature = input_weather.get_temperature(unit='celsius')['temp_min']
        w.visibility_distance = input_weather.get_visibility_distance()
        w.weather_code = input_weather.get_weather_code()
        w.weather_icon_name = input_weather.get_weather_icon_name()
        w.wind_deg = input_weather.get_wind()['deg']
        w.wind_speed = input_weather.get_wind()['speed']
        w.save()
        return w

    @classmethod
    def out_weather(cls, w: Weather):
        return {
            'Humidity': w.humidity,
            'DetailedStatus': w.detailed_status,
            'Temperature': w.temperature,
            'MaxTemperature': w.max_temperature,
            'MinTemperature': w.min_temperature,
            'VisibilityDistance': w.visibility_distance,
            'WeatherCode': w.weather_code,
            'WeatherIconName': w.weather_icon_name,
            'WindDeg': w.wind_deg,
            'WindSpeed': w.wind_speed,
            'CheckTime': w.check_time,
        }
