import datetime
import mongoengine


class Weather(mongoengine.Document):
    city_id = mongoengine.ObjectIdField(required=True)
    detailed_status = mongoengine.StringField(required=True)
    check_time = mongoengine.DateTimeField(required=True , default=datetime.datetime.now)
    temperature = mongoengine.IntField(required=True)
    min_temperature = mongoengine.IntField()
    max_temperature = mongoengine.IntField()
    humidity = mongoengine.IntField()
    weather_code = mongoengine.IntField()
    visibility_distance = mongoengine.IntField()
    weather_icon_name = mongoengine.StringField()
    wind_speed = mongoengine.FloatField()
    wind_deg = mongoengine.IntField()


    meta = {
        'db_alias': 'core',
        'collection': 'weather',
        'indexed': [
            'city_name',
        ],
        'ordered':[
            '-check_time'
        ]
    }
    pass
