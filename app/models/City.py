import datetime
import mongoengine


class City(mongoengine.Document):
    city_id = mongoengine.IntField(required=True)
    name = mongoengine.StringField(required=True)
    country = mongoengine.StringField(required=True)
    pos = mongoengine.GeoPointField(required=True)
    meta = {
        'db_alias': 'core',
        'collection': 'cities',
        'indexed': [
            'name',
        ],
    }
    pass
